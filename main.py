import openpyxl

inv_file = openpyxl.load_workbook("Companies_sheet.xlsx")
product_list = inv_file["Sheet1"]

product_per_supplier = {}
total_value_per_supplier = {}
product_under_100_inv = {}

for product_row in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product_num = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5)

    # Suppliers
    if supplier_name in product_per_supplier:
        current_num_products = product_per_supplier.get(supplier_name)
        product_per_supplier[supplier_name] = current_num_products + 1
    else:
        print("New supplier adding")
        product_per_supplier[supplier_name] = 1


    # Total value of inventory
    if supplier_name in total_value_per_supplier:
        current_total_value = total_value_per_supplier.get(supplier_name)
        total_value_per_supplier[supplier_name] = current_total_value + inventory * price
    else:
        total_value_per_supplier[supplier_name] = inventory * price


    # Logic for less then 100 inventory
    if inventory < 100:
        product_under_100_inv[product_num] = inventory


    # Total inventory price
    inventory_price.value = inventory * price



print("Inventory in Company:", product_per_supplier)
print("Total price of inventory per supplier:", total_value_per_supplier)
print("Products with price less than 100:", product_under_100_inv)

inv_file.save("Companies_sheet_changed_by_python.xlsx")